import {EventEmitter} from "../eventdispatchers/eventdispatcher.js";

/**
 * The View. View presents the model and provides
 * the UI events. The controller is attached to these
 * events to handle the user interaction.
 */
export class gameView extends EventEmitter {
    constructor(gameModel, elements) {
        super();
        this._model = gameModel;
        this._elements = elements;

        //attach model listeners
        gameModel.on('canvasClick', (e) => this.processCanvasClick(e));
        gameModel.on('modifiedShapesQtyModifier', (shapes_qty) => {
            elements.shapesQtyModifierInput.value = shapes_qty
        });
        gameModel.on('modifiedGravityModifier', (gravity) => {
            elements.gravityModifierInput.value = gravity
        });
        gameModel.on('shapesQtyIndicatiorRefresh', (e) => {
            elements.shapesQtyIndicator.value = e
        });
        gameModel.on('totalShapesAreaRefresh', (e) => {
            elements.surfaceAreaIndicator.value = e
        });


        // attach listeners to HTML controls
        elements.increaseShapesQtyBtn.addEventListener('click',
            () => this.emit('increaseShapesQtyBtnClicked'));
        elements.decreaseShapesQtyBtn.addEventListener('click',
            () => this.emit('decreaseShapesQtyBtnClicked'));

        elements.increaseGravityBtn.addEventListener('click',
            () => this.emit('increaseGravityBtnClicked'));
        elements.decreaseGravityBtn.addEventListener('click',
            () => this.emit('decreaseGravityBtnClicked'));
        elements.gravityModifierInput.addEventListener('input',
            (newGravityValue) => {
                this.emit('gravityModifierInput', newGravityValue)
            });
        elements.shapesQtyModifierInput.addEventListener('input',
            (newQtyValue) => {
                this.emit('shapesQtyModifierInput', newQtyValue)
            });

    }

    processCanvasClick(e) {
        //blink a border? for later development

    }

}
